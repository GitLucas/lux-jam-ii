﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryHandler : MonoBehaviour
{
    [SerializeField] private GameObject player1, player2, player1Image, player2Image;

    public void WinsPlayerOne()
    {
        DisablePlayers();
        player1Image.SetActive(true);
    }
    
    public void WinsPlayerTwo()
    {
        DisablePlayers();
        player2Image.SetActive(true);
    }

    private void DisablePlayers()
    {
        player1.SetActive(false);
        player2.SetActive(false);
        
        StartCoroutine(GoToMenu());
    }

    private IEnumerator GoToMenu()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);
    }
}

﻿using System;
using UnityEngine;

public class PlayerOne : CharacterMovement
{
    protected override void Update()
    {
        if (GamePaused) return;
        
        if (Input.GetButtonDown("P1_Fire"))
            Attack();
        
        if (Input.GetButtonDown("P1_HeavyFire"))
            HeavyAttack();

        if (Input.GetButtonDown("P1_Jump"))
            Jump();

        if (Input.GetButtonDown("P1_Block"))
            Block(true);
        else if (Input.GetButtonUp("P1_Block"))
            Block(false);
                
        base.Update();
    }

    protected override void FixedUpdate()
    {
        X = Input.GetAxis("P1_Horizontal");
        Y = Input.GetAxis("P1_Vertical");

        base.FixedUpdate();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalHitboxController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer playerSpriteRenderer;
    private void Update()
    {
        transform.localScale = playerSpriteRenderer.flipX ? 
            new Vector3(-1, 1, 1) : 
            new Vector3(1, 1, 1);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseHandler : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private KeyboardHandler pauseKeyboard;
    
    public static event Action<bool> GamePaused;
    private bool _gamePaused;

    private void Start()
    {
        pauseKeyboard.Active = false;
    }

    private void Update()
    {
        if (!Input.GetButtonDown("Pause")) return;
        HandlePause();
    }

    public void HandlePause()
    {
        _gamePaused = !_gamePaused;
        GamePaused?.Invoke(_gamePaused);
        pauseMenu.SetActive(_gamePaused);
        pauseKeyboard.Active = _gamePaused;
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformController : MonoBehaviour
{
    [SerializeField] private Text scoreOne, scoreTwo;
    [SerializeField] private Image logoOne, logoTwo;
    [SerializeField] private VictoryHandler victory;

    private Stopwatch _timerPlayerOne, _timerPlayerTwo;
    private bool _p1, _p2;

    private float _p1Score, _p2Score;

    private const float MaxScore = 45;

    private void Start()
    {
        _timerPlayerOne = gameObject.AddComponent<Stopwatch>();
        _timerPlayerTwo = gameObject.AddComponent<Stopwatch>();
        
        _timerPlayerOne.Begin();
        _timerPlayerTwo.Begin();

        _timerPlayerOne.TimeUpdated += HandlePlayerOneUpdate;
        _timerPlayerTwo.TimeUpdated += HandlePlayerTwoUpdate;
        
        _timerPlayerOne.Pause();
        _timerPlayerTwo.Pause();

        _p1Score = _p2Score = MaxScore;
        scoreTwo.text = scoreOne.text = $"{MaxScore:00}:00";
    }

    private void HandlePlayerOneUpdate(float seconds, float milliseconds)
    {
        _p1Score = MaxScore - seconds - 1;
        var ms = (1 - milliseconds) * 100;
        scoreOne.text = $"{_p1Score:00}:{ms:00}";
        
        logoOne.fillAmount = (MaxScore - _p1Score) / 45;

        if (_p1Score < 0)
            victory.WinsPlayerOne();
    }

    private void HandlePlayerTwoUpdate(float seconds, float milliseconds)
    {
        _p2Score = MaxScore - seconds - 1;
        var ms = (1 - milliseconds) * 100;
        scoreTwo.text = $"{_p2Score:00}:{ms:00}";
        
        
        logoTwo.fillAmount = (MaxScore - _p2Score) / 45;

        if (_p2Score < 0)
            victory.WinsPlayerTwo();
    }

    private void FixedUpdate()
    {
        if (_p1 && _p2)
        {
            _timerPlayerOne.Pause();
            _timerPlayerTwo.Pause();
            return;
        }
        
        if(_p1)
            _timerPlayerOne.Unpause();
        else
            _timerPlayerOne.Pause();
        
        if(_p2)
            _timerPlayerTwo.Unpause();
        else 
            _timerPlayerTwo.Pause();

        
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("1"))
        {
            _p1 = true;
        }
        
        if (other.CompareTag("2"))
        {
            _p2 = true;
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("1"))
        {
            _p1 = false;
        }
        
        if (other.CompareTag("2"))
        {
            _p2 = false;
        }
    }
    
    
    
}

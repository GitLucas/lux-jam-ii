﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] private bool teleportX, teleportY;
    [SerializeField] private Transform target;

    private Vector3 targetPosition;
    private void Start()
    {
        targetPosition = target.position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var pos = other.transform.position;

        if (teleportX) pos.x = targetPosition.x;
        if (teleportY) pos.y = targetPosition.y;

        other.transform.position = pos;
        
        other.GetComponentInChildren<TrailRenderer>().Clear();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttackHitboxesHandler : MonoBehaviour
{
    [SerializeField] private CharacterMovement player;
    [SerializeField] private Collider2D
        // base attack
        attackForward,
        // heavy attack
        heavyAttackHorizontal,
        heavyAttackUp,
        heavyAttackDown,
        // airbone base attack
        airboneAttackHorizontal,
        // airbone heavy attack
        heavyAirboneAttackHorizontal,
        heavyAirboneAttackUp,
        heavyAirboneAttackDownOne,
        heavyAirboneAttackDownTwo;

    private Collider2D[] _colliders;
    private bool _allInactive = true;
    
    private void Start()
    {
        _colliders = new []
        {
            // base attack
            attackForward,
            // heavy attack
            heavyAttackHorizontal,
            heavyAttackUp,
            heavyAttackDown,
            // airbone base attack
            airboneAttackHorizontal,
            // airbone heavy attack
            heavyAirboneAttackHorizontal,
            heavyAirboneAttackUp,
            heavyAirboneAttackDownOne,
            heavyAirboneAttackDownTwo,
        } ;
    }

    public void HandleIdle()
    {
        player.CanAttack = true;
        if(_allInactive) return;
        DisableHitboxes();
    }


    public void HorizontalBaseAttack()
    {
        attackForward.enabled = true;
        _allInactive = false;
    }

    public void HorizontalHeavyAttack()
    {
        heavyAttackHorizontal.enabled = true;
        _allInactive = false;
    }
    
    public void UpHeavyAttack() {
        heavyAttackUp.enabled = true;
        _allInactive = false;
    }
    
    public void DownHeavyAttack() {
        heavyAttackDown.enabled = true;
        _allInactive = false;
    }

    
    public void HorizontalAirboneBaseAttack() {
        airboneAttackHorizontal.enabled = true;
        _allInactive = false;
    }
    
    public void HorizontalAirboneHeavyAttack() {
        heavyAirboneAttackHorizontal.enabled = true;
        _allInactive = false;
    }
    
    public void UpAirboneHeavyAttack() {
        heavyAirboneAttackUp.enabled = true;
        _allInactive = false;
    }
    
    public void DownAirboneHeavyAttackOne() {
        heavyAirboneAttackDownOne.enabled = true;
        _allInactive = false;
    }
    
    public void DownAirboneHeavyAttackTwo() {
        heavyAirboneAttackDownTwo.enabled = true;
        _allInactive = false;
    }

    public void DisableHitboxes()
    {
        foreach (var c in _colliders) c.enabled = false;
        
        player.EnableAttacks();
        _allInactive = true;
    }
    
}

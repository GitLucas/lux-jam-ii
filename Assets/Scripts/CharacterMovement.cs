﻿using System;
using System.Collections;
using System.Linq.Expressions;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    private const float MaxHealth = 20;

    [SerializeField] private Collider2D myCollider;
    [SerializeField] private Transform myTransform;
    [SerializeField] private Rigidbody2D myRigidbody;
    [SerializeField] private SpriteRenderer mySpriteRenderer;
    [SerializeField] private Animator myAnimator;

    [Header("UI")] 
    [SerializeField] private SpriteRenderer healtBar;

    protected float X, Y;

    private const float JumpForce = 5f;
    private const float MovementSpeed = .125f;

    private int _health = 20;

    private bool _canJump, _canAttack, _isBlocking, _canMove, _isCrouching;

    protected bool GamePaused;
    public bool CanAttack { set => _canAttack = value; }
    public bool Crouching => _isCrouching;
    public bool Jumping => !_downCondition;

    private void Start()
    {
        _canJump = _canAttack = _canMove = true;
        _barMaxWidth = healtBar.size.x;

        _castDown = _castLeft = _castRight = true;
        PauseHandler.GamePaused += OnGamePaused;
    }

    private bool _castDown, _castLeft, _castRight;
    private bool _leftCondition, _rightCondition, _downCondition;

    protected virtual void Update()
    {
        if (GamePaused) return;
        
        var center = myTransform.position;

        _downCondition = _rightCondition = _leftCondition = false;

        if (_castDown)
        {
            var downLeftRay = Physics2D.Raycast(center + Vector3.left *.2f, Vector2.down, .55f);
            var downRightRay = Physics2D.Raycast(center + Vector3.right *.2f, Vector2.down, .55f);
            _downCondition = downLeftRay.collider || downRightRay.collider;
        }

        if (_castLeft)
        {
            var leftRay = Physics2D.Raycast(center, Vector2.left, .25f);
            _leftCondition = leftRay.collider;
            Debug.DrawRay(center, Vector3.left * .25f, Color.green);
        }

        if (_castRight)
        {
            var rightRay= Physics2D.Raycast(center, Vector2.right, .25f);
            _rightCondition = rightRay.collider;
        }

        var wallHanging = (_leftCondition || _rightCondition) && !_downCondition;
        _canJump = _downCondition || wallHanging;
        
        
        myAnimator.SetBool("Jumping", !_downCondition);
    }

    protected virtual void FixedUpdate()
    {
        if (GamePaused) return;
        
        var moving = Math.Abs(X) > .125f;
        if (moving) Move();
        myAnimator.SetBool("Walking", moving && _canMove);
        
        
        Crouch(Y < -.001f);
    }

    private void Move()
    {
        if (!_canMove) return;
        
        myAnimator.SetBool("Walking", true);
        
        var dir = Mathf.Sign(X);
        myTransform.position += dir * MovementSpeed * Vector3.right;
        mySpriteRenderer.flipX = dir < 0;
    }

    protected void Jump()
    {
        if (!_canJump) return;
        
        if(_isBlocking)
            Block(false);
        if(_isCrouching)
            Crouch(false);

        _castDown = !_downCondition;
        _castLeft = !_leftCondition;
        _castRight = !_rightCondition;
        
        myRigidbody.velocity = Vector2.zero;
        myRigidbody.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
        _canJump = false;

    }

    private void Crouch(bool c)
    {
        if (Math.Abs(myRigidbody.velocity.y) > .001f) return;
        
        if (c && !_isCrouching)
        {
            _isCrouching = true;
            myAnimator.SetBool("Crouching", true);
            _canMove = false;
            return;
        }

        if (c || !_isCrouching) return;
        
        
        _isCrouching = false;
        _canMove = true;
        myAnimator.SetBool("Crouching", false);
    }

    protected void Block(bool b)
    {
        if (b && !_canJump) return;
        
        _isBlocking = b;
        myAnimator.SetBool("Blocking", b);
        _canMove = _canAttack = !b;
    }

    protected void Attack()
    {
        if (!_canAttack) return;
        
        _canAttack = false;
        _canMove = !_canJump;

        myAnimator.SetTrigger("AttackHorizontal");

    }

    protected void HeavyAttack()
    {
        if (!_canAttack) return;
        
        _canAttack = false;
        _canMove = !_canJump;
        myAnimator.SetBool("Walking", false);

        
        if (Y < -0.011f)
        {
            myAnimator.SetTrigger("HeavyAttackDown");
            return;
        }
        
        if (Y > 0.011f)
        {
            myAnimator.SetTrigger("HeavyAttackUp");
            return;
        }
        
        myAnimator.SetTrigger("HeavyAttackHorizontal");
    }

    public void EnableAttacks()
    {
        _canAttack = true;
        _canMove = true;
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (_canJump) return;

        var downLeftRay = Physics2D.Raycast(myTransform.position + Vector3.left *.2f, Vector2.down, .55f);
        var downRightRay = Physics2D.Raycast(myTransform.position + Vector3.right *.2f, Vector2.down, .55f);
        if (downLeftRay.collider || downRightRay.collider)
        {
            _castDown = _castLeft =
                _castRight = true;
        }
    }

    private float _barWidth, _barMaxWidth;
    public void Hit(int dmg, bool ignoreBlock, bool downAttack, bool upAttack, bool heavy)
    {
        if (_isBlocking &&
            (_isCrouching && upAttack ||
             !_isCrouching && downAttack ||
             !_isCrouching && !upAttack && !downAttack)) dmg = heavy ? 1 : 0;
        
        if (_isBlocking && ignoreBlock)
            Block(false);

        if(_isBlocking && dmg > 1) 
            return;
        
        _health -= dmg;

        var healtBarSize = healtBar.size;
        if (_health > 0)
        {
            var per = _health / MaxHealth;
            healtBarSize.x = _barMaxWidth * per;
            healtBar.size = healtBarSize;
        }
        else
        {
            // Player dies
            healtBar.size = new Vector2(_barMaxWidth, healtBarSize.y);
            transform.localPosition = Vector3.zero;
            _health = (int) MaxHealth;
        }
    }
    
    private Vector2 _vel;

    private void OnGamePaused(bool paused)
    {
        myRigidbody.isKinematic = paused;
        myCollider.enabled = !paused;
        GamePaused = paused;
        
        if (paused)
        {
            _vel = myRigidbody.velocity;
            myRigidbody.velocity = Vector2.zero;
            myAnimator.speed = 0;
        }
        else
        {
            myRigidbody.velocity = _vel;
            myAnimator.speed = 1;
        }
        
        
    }

    private void OnDestroy()
    {
        PauseHandler.GamePaused -= OnGamePaused;
    }
}

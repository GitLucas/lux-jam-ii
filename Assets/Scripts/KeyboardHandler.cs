﻿using System;
using System.Collections;
using UnityEngine;

public class KeyboardHandler : MonoBehaviour
{
    [SerializeField] private KeyboardButton[] buttons;

    public bool Active
    {
        get => _active;
        set => _active = value;
    }

    private int _index;
    private float _y;
    private bool _active = true;

    private void Start()
    {
        buttons[0].Select();
    }

    private void Update()
    {
        if (!_active) return;
        
        _y = Input.GetAxis("P1_Vertical");

        if (Input.GetButtonDown("P1_Vertical") || Mathf.Abs(_y) > .5f)
            HandleMovement();

        if (Input.GetButtonDown("P1_Jump"))
        {
            buttons[_index].Interact();
            _active = false;
        }
    }

    private bool _moving;

    private void HandleMovement()
    {
        if (_moving) return;
        
        _moving = true;
        buttons[_index].Deselect();
        var dir = (int) Mathf.Sign(_y);
        _index = (buttons.Length + _index + dir) % buttons.Length;
        buttons[_index].Select();

        StartCoroutine(EnableMoving());

        IEnumerator EnableMoving()
        {
            yield return new WaitForSeconds(.25f);
            _moving = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyboardButton : MonoBehaviour
{
    [SerializeField] private Button btn;
    [SerializeField] private GameObject arrow;

    public void Select()
    {
        arrow.SetActive(true);
    }

    public void Deselect()
    {
        arrow.SetActive(false);
    }

    public void Interact()
    {
        btn.onClick.Invoke();
    }
}

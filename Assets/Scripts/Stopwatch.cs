﻿//Written by Daniel Keele - 6/6/2019
using System;
using UnityEngine;

public class Stopwatch : MonoBehaviour
{
    private float elapsedRunningTime = 0f;
    private float runningStartTime = 0f;
    private float pauseStartTime = 0f;
    private float elapsedPausedTime = 0f;
    private float totalElapsedPausedTime = 0f;
    private bool running = false;
    private bool paused = false;

    public event Action<float, float> TimeUpdated;
      
    private void Update()
    {
        if (running)
        {
            elapsedRunningTime = Time.time - runningStartTime - totalElapsedPausedTime;
            TimeUpdated?.Invoke(GetSeconds(), GetMilliseconds());
        }
        else if (paused)
        {
            elapsedPausedTime = Time.time - pauseStartTime;
        }
    }

    public void Begin()
    {
        if (running || paused) return;
        
        runningStartTime = Time.time;
        running = true;
    }

    public void Pause()
    {
        if (!running || paused) return;
        
        running = false;
        pauseStartTime = Time.time;
        paused = true;
    }

    public void Unpause()
    {
        if (running || !paused) return;
        
        totalElapsedPausedTime += elapsedPausedTime;
        running = true;
        paused = false;
    }

    public void Reset()
    {
        elapsedRunningTime = 0f;
        runningStartTime = 0f;
        pauseStartTime = 0f;
        elapsedPausedTime = 0f;
        totalElapsedPausedTime = 0f;
        running = false;
        paused = true;
    }

    public int GetMinutes()
    {
        return (int)(elapsedRunningTime / 60f);
    }

    public int GetSeconds()
    {
        return (int)(elapsedRunningTime % 60);
    }

    public float GetMilliseconds()
    {
        return (float)(elapsedRunningTime - Math.Truncate(elapsedRunningTime));
    }

    public float GetRawElapsedTime()
    {
        return elapsedRunningTime;
    }
}
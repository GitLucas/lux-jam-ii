﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    [SerializeField] private CharacterMovement player;
    [SerializeField] private int damage = 1;
    [SerializeField] private bool ignoreBlock;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var p = other.GetComponent<CharacterMovement>();

        if (!p || p == player) return;
        
        p.Hit(damage, ignoreBlock, player.Crouching, player.Jumping, damage > 3);
    }
}

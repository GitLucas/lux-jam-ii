﻿using System;
using UnityEngine;

public class PlayerTwo : CharacterMovement
{
    protected override void Update()
    {
        if (GamePaused) return;

        if (Input.GetButtonDown("P2_Fire"))
            Attack();
        
        if (Input.GetButtonDown("P2_HeavyFire"))
            HeavyAttack();
        
        if (Input.GetButtonDown("P2_Jump"))
            Jump();
        
        if (Input.GetButtonDown("P2_Block"))
            Block(true);
        else if (Input.GetButtonUp("P2_Block"))
            Block(false);
            
        base.Update();
    }

    protected override void FixedUpdate()
    {
        X = Input.GetAxis("P2_Horizontal");
        Y = Input.GetAxis("P2_Vertical");
        
        base.FixedUpdate();
    }
    
}
